# MPS-scrambled-strings


## Name
Scrambled Strings

## Description
This project was created for MPS and Pepperstone. I contains an implementation of the "Scrambled strings" problem described in the Data Team Code Challange document.

## Installation
To run this script You will need python 3 and pandas. If You do not have them, please consider installing them:
$sudo apt-get install python3
$sudo pip3 install pandas

Clone the repo into an arbitrary folder:
$sudo git clone https://gitlab.com/maarcsab/mps-scrambled-strings.git

(No need to create a build)

## Usage
$python3 scramled-strings.py --dictionary [dict_file_with_full_path] --input [input_file_with_full_path]
eg.: python3 scramled-strings.py --dictionary dict.txt --input search.txt

The attached dict.txt contains the dictionary, and the search.txt containt the string we are searching in.

There is a config.ini file within the package. You can set the paratmeter "verbose_log" to "True" for detailed debug info (in this case the elapsed time measurements are not relevant). 
Perheps this mode gives a better insight how the implemented algorithms work.

The script will put the result to the standard output.  (No separate output file created)

## Authors and acknowledgment
Maár Csaba


