import configparser
import logging
import sys
import os.path
import time
import pandas as pd

# ----------------------------------------------
# set up logging
vConfig = configparser.ConfigParser()
vConfig.read('config.ini')
vDefConfig = vConfig['DEFAULT']
        
vLogger = logging.getLogger()
if vDefConfig.getboolean('verbose_log'):
    vLogger.setLevel(logging.DEBUG)
else:
    vLogger.setLevel(logging.INFO)

vHandler = logging.StreamHandler(sys.stdout)

if vDefConfig.getboolean('log_date_prefix'):
    vFormatter = logging.Formatter('[%(asctime)s;%(name)s;%(levelname)s] %(message)s')
else:
    vFormatter = logging.Formatter('[%(levelname)s] %(message)s')
vHandler.setFormatter(vFormatter)
vLogger.addHandler(vHandler)

if vDefConfig.getboolean('verbose_log'):
    vLogger.info('Log level: DEBUG')
else:
    vLogger.info('Log level: INFO')


# ----------------------------------------------
# implementing search
class Pepperstone:
    
    def __init__(self, pDict, pSearch, pLogger):
        if not os.path.exists(pDict):
            pLogger.error('Dictionary file: {} does not exist'.format(pDict))
            return
        
        if not os.path.exists(pSearch):
            pLogger.error('Search file: {} does not exist'.format(pSearch))
            return
        
        self.vDictFile = open(pDict, 'r')
        self.vSearchFile = open(pSearch, 'r')
        self.vLogger = pLogger
        self.vRunningFreq = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        self.vRes = {}
        
        
        self.vDistinctDict = pd.DataFrame()
        
    ''' --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- '''    
    ''' some common functions '''    
    
    ''' ---------------------------------------------------------------------------------
        to add some daley when debugging (only)
    '''     
    def logger_delay(self):
        if self.vLogger.level == logging.DEBUG:
           time.sleep(1) 
    
    ''' ---------------------------------------------------------------------------------
        generating frequency array for a dictionary word
        the frequency array is a simple list of 26 elements, reresenting the characters
        between a - z
    '''
    def get_frequency_array(self, pStr):
        vFreqArr = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        for chr in pStr.lower():
            vFreqArr[ord(chr)-97]+=1
        return vFreqArr    
            
    ''' ---------------------------------------------------------------------------------
        updateing the runningfrequency array, decreasing the occurance of the "leaving"
        character, and increasingthe occurance of thenew character
    ''' 
    def upd_running_freq(self, pRunningFreq, pOut, pIn):
        pRunningFreq[ord(pOut)-97]-=1
        pRunningFreq[ord(pIn)-97]+=1
        
        
    ''' ---------------------------------------------------------------------------------
        clearing the result set
    ''' 
    def clear_res(self):
        self.vRes = {}
    
    ''' --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- '''    
    ''' version 1 '''
    
    ''' ---------------------------------------------------------------------------------
        searching for the dict word in the search text, based on the frequency array
        we dot not have to iterate through all on the search text, we can exit from the iteration
        at the first hit. We need just to log if the word present in the text at all
    '''
    def search_for_dict(self, pDict, pSearch, pCase):
    
        vRunningFreq = []
        vDictLen = len(pDict)
        vDictFreq = self.get_frequency_array(pDict)
        vI = 0
        vFound = False;
        
        ''' --- some logging ----------------------------------------------------------- '''
        self.vLogger.debug('dict word: {}, word length: {}'.format(pDict, vDictLen))
        self.vLogger.debug('dict word freq array: {}'.format(vDictFreq))
        ''' --- some logging ----------------------------------------------------------- '''
        
        vRunningFreq = self.get_frequency_array(pSearch[vI:vI+vDictLen])
        
        ''' --- some logging ----------------------------------------------------------- '''
        self.vLogger.debug('search text running freq array: {}'.format(vRunningFreq))
        ''' --- some logging ----------------------------------------------------------- '''
        
        while not vFound and vI < len(pSearch)-vDictLen:
            ''' --- some logging ----------------------------------------------------------- '''
            self.vLogger.debug('comparison of dict word freq array and running freq array:')
            self.vLogger.debug(vDictFreq)
            self.vLogger.debug(vRunningFreq)
            self.logger_delay()
            ''' --- some logging ----------------------------------------------------------- '''
            
            if     vRunningFreq == vDictFreq \
               and pDict[0]  == pSearch[vI] \
               and pDict[-1] == pSearch[vI+vDictLen-1]:
               
                ''' --- some logging ----------------------------------------------------------- '''
                self.vLogger.debug('we have a match, exiting from iteration')
                ''' --- some logging ----------------------------------------------------------- '''
                vFound = True
                self.vRes[pCase]+=1
            else:               
                self.upd_running_freq(vRunningFreq, pSearch[vI], pSearch[vI+vDictLen])
                vI+=1
    
    ''' ---------------------------------------------------------------------------------
        iterating thorugh the dictionary words and seacrh strings
    '''
    def scrmabled_strings(self):
    
        self.vSearchFile.seek(0) 
        self.vDictFile.seek(0) 
    
        vI = 1 
        #reading line from the search string file
        vStart = time.time()
        for line in self.vSearchFile:
            line = line.rstrip()
            vCase = 'Case #'+str(vI)
            self.vRes[vCase]=0
            vI+=1
            
            for dict in self.vDictFile:
                vDict = dict.rstrip()
                
                self.search_for_dict(vDict, line, vCase)
                ''' --- some logging ----------------------------------------------------------- '''
                self.vLogger.debug('')
                self.vLogger.debug('')
                ''' --- some logging ----------------------------------------------------------- '''
        
        vElapsedTime = time.time() - vStart
        
        # print result
        self.vLogger.info('')
        self.vLogger.info('')
        self.vLogger.info('--------------------------------------------')
        self.vLogger.info('The version iterating through the dictionary words and search strings:')
        self.vLogger.info('Elapsed time: {}'.format(vElapsedTime))
        self.vLogger.info(self.vRes)
        self.vLogger.info('--------------------------------------------')
        
        
        
    ''' --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- '''    
    ''' version 2 '''


    ''' ---------------------------------------------------------------------------------
        generating the list of distinct word lengths
        
        also we can check for duplicated words (first and last character equals, and freq 
        array equals), we can aggregate them and count them in time of occurance (don't have
        to iterate multiple time forthe same word)
        
        reading the file line by line, due to the possible large number of dictionary words
        (don't want to store the while file in the memory)
        storing the distinct dictionary in a DataFrame, not sure how efficient it is, will see
        pandas will sort the aggregated output
        
        in one shot I create and store the freq array as well for each word
    '''
    def get_list_of_distinct_length(self):    
        for line in self.vDictFile:
            vDict = line.rstrip() 
            vLen = len(vDict)
            vDictFreq = self.get_frequency_array(vDict)
            
            self.vDistinctDict = self.vDistinctDict.append(pd.DataFrame([{'length': vLen, 'first_letter':vDict[0], 'last_letter': vDict[-1], 'freq_array': ','.join(map(str, vDictFreq))}]), ignore_index=True)
            
        self.vLogger.debug(self.vDistinctDict)
        self.vDistinctDict = self.vDistinctDict.groupby(["length", "first_letter", "last_letter", "freq_array"], as_index=False).agg(counter = pd.NamedAgg(column="first_letter", aggfunc='count'))

        self.vLogger.debug(self.vDistinctDict)
    
    
    ''' ---------------------------------------------------------------------------------
        searching for the dict words in the search text, based on the length
        we dot not have to iterate through all on the search text, we can exit from the iteration
        at the first hit. We need just to log if the word present in the text at all
        We need to add the occurance we calculatedfor the dict word
    '''
    def search_for_dicts_with_length(self, pLen, pSearch, pCase):
    
        vRunningFreq = []
        vI = 0
        vFound = False;
        
        vRunningFreq = self.get_frequency_array(pSearch[vI:vI+pLen])
        
        ''' --- some logging ----------------------------------------------------------- '''
        self.vLogger.debug('search text running freq array: {}'.format(vRunningFreq))
        ''' --- some logging ----------------------------------------------------------- '''
        
        vDictWords = self.vDistinctDict.loc[self.vDistinctDict["length"] == pLen]
        ''' --- some logging ----------------------------------------------------------- '''
        self.vLogger.debug('possible dict words: {}'.format(vDictWords))
        ''' --- some logging ----------------------------------------------------------- '''
        
        while vI < len(pSearch)-pLen and len(vDictWords.index) > 0:
            for index, row in vDictWords.iterrows():
                vFirstLetter=row["first_letter"]
                vLastLetter=row["last_letter"]
                vDictFreq=list(map(int, row["freq_array"].split(',')))
                vCount=row["counter"]
                
                ''' --- some logging ----------------------------------------------------------- '''
                self.vLogger.debug('comparison of dict word first/last array and running leters:')
                self.vLogger.debug('first letter: {}, last letter: {}'.format(vFirstLetter, vLastLetter))
                self.vLogger.debug('first letter: {}, last letter: {}'.format(pSearch[vI], pSearch[vI+pLen-1]))
                self.vLogger.debug('')
                self.vLogger.debug('comparison of dict word freq array and running freq array:')
                self.vLogger.debug(vDictFreq)
                self.vLogger.debug(vRunningFreq)
                self.vLogger.debug('')
                self.logger_delay()
                ''' --- some logging ----------------------------------------------------------- '''
            
                if     vRunningFreq == vDictFreq \
                   and vFirstLetter  == pSearch[vI] \
                   and vLastLetter == pSearch[vI+pLen-1]:
               
                    ''' --- some logging ----------------------------------------------------------- '''
                    self.vLogger.debug('we have a match, exiting from iteration')
                    ''' --- some logging ----------------------------------------------------------- '''
                    vDictWords = vDictWords.drop(index)
                    self.vRes[pCase]+=vCount

            self.upd_running_freq(vRunningFreq, pSearch[vI], pSearch[vI+pLen])
            vI+=1    
        
    ''' ---------------------------------------------------------------------------------
      2nd version, iterate on the distinct words
    '''
    def scrmabled_strings_dist(self):
    
        self.vSearchFile.seek(0) 
        self.vDictFile.seek(0) 
    
        vStart = time.time()
        
        # generating the distict dictionary
        self.get_list_of_distinct_length()
        
        vI = 1 
        #reading line from the search string file
        for line in self.vSearchFile:
            line = line.rstrip()
            vCase = 'Case #'+str(vI)
            self.vRes[vCase]=0
            vI+=1
            
            vLengths = self.vDistinctDict["length"].unique()
            
            for length in vLengths:
                
                self.search_for_dicts_with_length(length, line, vCase)
                ''' --- some logging ----------------------------------------------------------- '''
                self.vLogger.debug('')
                self.vLogger.debug('')
                ''' --- some logging ----------------------------------------------------------- '''
        
        vElapsedTime = time.time() - vStart
        
        # print result
        self.vLogger.info('')
        self.vLogger.info('')
        self.vLogger.info('--------------------------------------------')
        self.vLogger.info('The version calculating the distinct lengths and iterating on the lengths:')
        self.vLogger.info('Elapsed time: {}'.format(vElapsedTime))
        self.vLogger.info(self.vRes)
        self.vLogger.info('--------------------------------------------')    

if __name__ == '__main__':
    
    if len(sys.argv) == 5:
        vDict = None
        vSearch = None
        if sys.argv[1] == '--dictionary':
            vDict = sys.argv[2]
        if sys.argv[3] == '--dictionary':    
            vDict = sys.argv[4]
            
        if sys.argv[1] == '--input':
            vSearch = sys.argv[2]
        if sys.argv[3] == '--input':    
            vSearch = sys.argv[4]   

        if vDict is not None and vSearch is not None:
            vSS = Pepperstone(vDict, vSearch, vLogger)
            vSS.scrmabled_strings_dist()
            vSS.clear_res()
            vSS.scrmabled_strings()
    else:
        vLogger.error('Mandatory arguments are missing. command usage: $python3 scramled-strings.py --dictionary [dict_file_with_full_path] --input [input_file_with_full_path]')
        